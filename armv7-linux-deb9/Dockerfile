FROM arm32v7/debian:stretch

ENV LANG C.UTF-8

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

# Core build utilities
RUN apt-get update \
    && apt-get install --no-install-recommends -qy zlib1g-dev libtinfo-dev libgmp10 libgmp-dev \
    libsqlite3-0 libsqlite3-dev \
    ca-certificates g++ git make automake autoconf gcc \
    perl python3 texinfo xz-utils lbzip2 bzip2 patch openssh-client sudo time \
    jq wget curl \
    # Documentation tools
    python3-sphinx texlive-xetex texlive-latex-extra \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/*

# Boot LLVM
ENV PATH /usr/local/bin:$PATH
ENV LLVM_TARBALL http://releases.llvm.org/5.0.1/clang+llvm-5.0.1-armv7a-linux-gnueabihf.tar.xz
RUN curl $LLVM_TARBALL | tar -xJC .. && \
    mkdir /opt/llvm5 && \
    cp -R clang+llvm*/* /opt/llvm5 && \
    rm -R clang+llvm* && \
    /opt/llvm5/bin/llc --version

# GHC
RUN curl http://home.smart-cactus.org/~ben/ghc/ghc-8.4.4-arm-unknown-linux.tar.xz | tar -xJ
#RUN curl http://downloads.haskell.org/~ghc/8.6.2/ghc-8.6.2-aarch64-ubuntu_bionic-linux.tar.xz | tar -xJ
WORKDIR /ghc-8.4.4
RUN ./configure --prefix=/usr/local LLC=/opt/llvm5/bin/llc OPT=/opt/llvm5/bin/opt && \
    make install
WORKDIR /
RUN rm -Rf ghc-*
RUN ghc --version

# LLVM
ENV LLVM_TARBALL http://releases.llvm.org/7.0.0/clang+llvm-7.0.0-armv7a-linux-gnueabihf.tar.xz
ENV LLC /opt/llvm7/bin/llc
ENV OPT /opt/llvm7/bin/opt
RUN curl $LLVM_TARBALL | tar -xJC .. && \
    mkdir /opt/llvm7 && \
    cp -R clang+llvm*/* /opt/llvm7 && \
    rm -R clang+llvm* && \
    $LLC --version

# Cabal
RUN git clone https://github.com/haskell/Cabal
WORKDIR /Cabal
RUN git checkout cabal-install-v2.4.1.0
WORKDIR /Cabal/cabal-install
RUN EXTRA_CONFIGURE_OPTS=--disable-optimization ./bootstrap.sh --global --no-doc

RUN ls /Cabal/cabal-install/dist/build

ENV PATH /home/ghc/.local/bin:/opt/cabal/2.2/bin:/opt/ghc/8.4.2/bin:$PATH

# Create a normal user.
RUN adduser ghc --gecos "GHC builds" --disabled-password
RUN echo "ghc ALL = NOPASSWD : ALL" > /etc/sudoers.d/ghc
USER ghc
WORKDIR /home/ghc/

# Build Haskell tools
RUN cabal update && \
    cabal install hscolour happy alex --constraint 'happy ^>= 1.19.10'
ENV PATH /home/ghc/.cabal/bin:$PATH

CMD ["bash"]

